﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
// code https://wiki.unity3d.com/index.php/Singleton
public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    
    private static bool isShuttingDown = false;
    private static object lockObject = new object();
    private static T instance;
 
    /// <summary>
    /// Access singleton instance through this propriety.
    /// </summary>
    public static T Instance
    {
        get
        {
            if (isShuttingDown)
            {
                return null;
            }
 
            lock (lockObject)
            {
                if (Instance == null)
                {
                    Instance = FindObjectOfType<T>();
 
                   
                    if (Instance == null)
                    {
                      
                        var singleton = new GameObject();
                        Instance = singleton.AddComponent<T>();
                        singleton.name = instance.GetType().Name;
                        DontDestroyOnLoad(singleton);
                    }
                }
 
                return Instance;
            }
        }
        set => throw new NotImplementedException();
    }


    private void OnApplicationQuit()
    {
        isShuttingDown = true;
    }
 
 
    private void OnDestroy()
    {
        isShuttingDown = true;
    }
}
