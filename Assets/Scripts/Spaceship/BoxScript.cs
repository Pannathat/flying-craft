﻿using UnityEngine;


public class BoxScript : MonoBehaviour
{
    public float speed = 1f;
    Vector3 objStart;
    void Start()
    {
        objStart = transform.position;
    }
    void FixedUpdate()
    {
        transform.Translate (Vector3.left * speed * Time.deltaTime);
    }
    void OnBecameInvisible()
    {
        transform.position = objStart;
    }

   
}
