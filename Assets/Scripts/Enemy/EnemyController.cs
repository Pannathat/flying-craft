﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy 
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] public EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;

        void FixedUpdate()
        {
           
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
            
         }
         
    }    
}

